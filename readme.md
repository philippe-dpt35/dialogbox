# Présentation

La fonction alert() de javascript ne permet d'afficher que du texte.

L'outil suivant permet d'afficher une boîte de dialogue, incluant ou non une image. L'image peut être insérée à gauche, à droite, en haut ou en bas du texte. La boîte de dialogue et tous ses éléments seront affichés et positionnés en fonction de la taille du contenu.

## Copies d'écran

Les fichiers html de chaque dossier permettent de tester la boîte de dialogue.

`showDialog('Bravo !', 0.5,'happy-tux.png', 90, 90, 'left');`

![5c3e5f361bf85](https://i.loli.net/2019/01/16/5c3e5f361bf85.png)

`showDialog('Bravo !', 0.5,'happy-tux.png', 90, 90, 'right');`

![5c3e5f4fbdeaf](https://i.loli.net/2019/01/16/5c3e5f4fbdeaf.png)

`showDialog('Bravo !', 0.5,'happy-tux.png', 90, 90, 'bottom');`

![5c3e5f5d295c8](https://i.loli.net/2019/01/16/5c3e5f5d295c8.png)

`showDialog('Bravo !', 0.5,'happy-tux.png', 90, 90, 'top');`

![5c3e5f8079592](https://i.loli.net/2019/01/16/5c3e5f8079592.png)

`showDialog('Candes-Saint-Martin, parfois abrégé « Candes » dans le langage courant, <br/>est une commune française du département d\'Indre-et-Loire,<br/>en région Centre-Val de Loire.', 0.5,'Candes.jpg', 310, 175, 'left');`

![5c3e60375184d](https://i.loli.net/2019/01/16/5c3e60375184d.png)

`showDialog('Candes-Saint-Martin, parfois abrégé « Candes » dans le langage courant, <br/>  est une commune française du département d\'Indre-et-Loire,<br/>  en région Centre-Val de Loire.', 0.5,'Candes.jpg', 310, 175, 'right');`

![5c3e6061088ed](https://i.loli.net/2019/01/16/5c3e6061088ed.png)

`showDialog('Candes-Saint-Martin, parfois abrégé « Candes » dans le langage courant, <br/> est une commune française du département d\'Indre-et-Loire,<br/> en région Centre-Val de Loire.', 0.5,'Candes.jpg', 310, 175, 'bottom');`

![5c3e608ea5385](https://i.loli.net/2019/01/16/5c3e608ea5385.png)

`showDialog('Candes-Saint-Martin, parfois abrégé « Candes » dans le langage courant, <br/>  est une commune française du département d\'Indre-et-Loire,<br/>  en région Centre-Val de Loire.', 0.5,'Candes.jpg', 310, 175, 'top');`

![5c3e609cd51c9](https://i.loli.net/2019/01/16/5c3e609cd51c9.png)

## Usage

### Paramètres

Les paramètres à passer sont les suivants:

    ("Chaine de caractères à afficher" [, Opacite, cheminImg, largeurImage, hauteurImage, Position])

Il est possible de ne passer que le premier paramètre pour n'afficher que du texte.

**Opacite** : valeur numérique, opacité de l'arrière-plan (1 donne un arrière-plan invisible)

**cheminImg** : chaîne de caractères

**largeurImage** : valeur numérique, largeur de l'image en pixels

**hauteurImage** : valeur numérique, hauteur de l'image en pixels

**Position** : chaîne de caractère ayant les valeurs "left", "top", "right" ou "bottom"

La largeur et la hauteur d'affichage de l'image peuvent différer des dimensions réelles de l'image. L'image sera alors redimensionnée en fonction des valeurs entrées tout en conservant ses proportions.

### Code javascript

#### Version fonction

    showDialog('Bravo !',0.5,'happy-tux.png', 80, 80, 'right')";

#### Version objet

    let Bravo = new dialogBox('Bravo !',0.5,'happy-tux.png', 80, 80, 'right');
    Bravo.showDialog();

L'apparence de la boîte de dialogue peut être très facilement personnalisée en intervenant sur le fichier dialogbox.css.

Les principes de cette boîte de dialogue ont été inspirés de l'outil suivant : https://github.com/yusufshakeel/Web-App/tree/master/DialogBox
