function dialogBox(Texte, Opacite = "0.5", pathImg = '', imgWidth = 0, imgHeight = 0, Place = 'left') {
var hideScreen;
var dlgBox;
var dlgBody;
var dlgFooter;
var dlgBoxWidth;
var dlgBoxHeight;
var dlgButton;

  function buildElements() {
    hideScreen = document.createElement("div");
    hideScreen.id = "hide-screen";
    hideScreen.style.display = "none";
    dlgBox = document.createElement("div");
    dlgBox.id = "dlg-box";
    dlgBox.style.display = "none";
    dlgBody = document.createElement("div");
    dlgBody.id = "dlg-body";
    dlgFooter = document.createElement("div");
    dlgFooter.id = "dlg-footer";
    dlgButton = document.createElement("button");
    dlgButton.addEventListener("click", dlgExit);
    dlgButton.innerHTML = "OK";
    dlgFooter.appendChild(dlgButton);
    document.body.appendChild(hideScreen);
    document.body.appendChild(dlgBox);
    dlgBox.appendChild(dlgBody);
    dlgBox.appendChild(dlgFooter);
  }

  function positionDlgBox() {
    let winWidth = window.innerWidth;
    let winHeight = window.innerHeight;
    dlgBox.style.left = 50 - (dlgBoxWidth / winWidth) * 50 + "%" ;
    dlgBox.style.width = dlgBoxWidth + "px";
    dlgBox.style.top = 50 - (dlgBoxHeight / winHeight) * 50 - 10 + "%" ;
  }

  function dlgExit() {
    document.body.removeChild(dlgBox);
    document.body.removeChild(hideScreen);
  }

  this.showDialog = function() {
    buildElements();
    let dlgText = document.createElement("div");
    dlgText.id = "dlg-text";
    let dlgImage = document.createElement("div");
    dlgImage.id = "dlg-image";
    dlgImage.style.backgroundImage = "url(" + pathImg + ")";
    dlgImage.style.backgroundSize = "contain";
    dlgImage.style.backgroundRepeat = "no-repeat";
    dlgImage.style.backgroundPosition = "center";
    hideScreen.style.display = "block";
    hideScreen.style.opacity = Opacite;
    dlgBox.style.display = "flex";
    dlgBox.style.flexDirection = "column";
    // Détermine les dimensions de la boîte de dialogue avant insertion de l'image
    dlgText.innerHTML = Texte;
    dlgBody.appendChild(dlgText);
    dlgBoxWidth = dlgBox.clientWidth;
    dlgBoxHeight = dlgBox.clientHeight;
    switch(Place) {
      case "left":
        dlgBoxWidth += Img;
        if (imgHeight > dlgBoxHeight) { dlgBoxHeight =  imgHeight;}
        dlgBody.insertBefore(dlgImage, dlgText);
        break;
      case "right":
        dlgBoxWidth += imgWidth;
        if (imgHeight > dlgBoxHeight) { dlgBoxHeight =  imgHeight;}
        dlgBody.appendChild(dlgImage);
        break;
      case "top":
        dlgBoxHeight +=  imgHeight;
        if (imgWidth > dlgBoxWidth) { dlgBoxWidth =  imgWidth;}
        dlgBody.style.flexDirection = "column";
        dlgBody.insertBefore(dlgImage, dlgText);
        break;
      case "bottom":
        dlgBoxHeight += imgHeight;
        if (imgWidth > dlgBoxWidth) { dlgBoxWidth =  imgWidth;}
        dlgBody.style.flexDirection = "column";
        dlgBody.appendChild(dlgImage);
        break;
    }
    dlgImage.style.width = imgWidth + "px";
    dlgImage.style.height = imgHeight + "px";
    positionDlgBox();
  }
}
